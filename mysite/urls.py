"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from django.contrib import admin
from baton.autodiscover import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from geriatrics.admin import geriatrics_admin_site
from kardex.admin import kardex_admin_site
from laboratories.admin import laboratories_admin_site
from medication.admin import medication_admin_site
from patient_monitoring.admin import patient_monitoring_admin_site 
from plan_of_care.admin import plan_of_care_admin_site
from scanned_docs.admin import scanned_docs_admin_site
from health_declaration.admin import health_declaration_admin_site
from central_supplies.admin import central_supplies_admin_site
from orders.admin import orders_admin_site

urlpatterns = [
    path('admin/', admin.site.urls),
    path('baton/', include('baton.urls')),
]

# Admin
urlpatterns += [
    path('geriatrics/admin/', geriatrics_admin_site.urls),
    path('kardex/admin/', kardex_admin_site.urls),
    path('laboratories/admin/', laboratories_admin_site.urls),
    path('medication/admin/', medication_admin_site.urls),
    path('patient_monitoring/admin/', patient_monitoring_admin_site.urls),
    path('plan_of_care/admin/', plan_of_care_admin_site.urls),
    path('scanned_docs/admin/', scanned_docs_admin_site.urls),
    path('health_declaration/admin/', health_declaration_admin_site.urls),
    path('central_supplies/admin/', central_supplies_admin_site.urls),
    path('orders/admin/', orders_admin_site.urls),
]

urlpatterns += [
    path('', include('cm_portal.urls')),
    path('account/', include('account.urls')),
    path('geriatrics/', include('geriatrics.urls')),
    #path('monitoring/', include('patient_monitoring.urls')),
    path('health_declaration/', include('health_declaration.urls')),
]

urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT)
                      
admin.site.site_header = "JHWC access Admin"
admin.site.site_title = "JHWC access Admin Portal"
admin.site.index_title = "Welcome to JHWC access"
