document.addEventListener('DOMContentLoaded', () => {
  const insertHere = document.querySelector('#insert-here');
  const urlPath = 'admissions/?';
  const urlPathDC = 'admissions/?dc&';
  
  function searchSubmit() {
  	 console.log('running search');
    let query = $('#id_query').val();
    $(insertHere).load('admissions/?query=' + encodeURIComponent(query));
    $('#admissions_pill').attr('class', 'nav-link');
    $('#discharged_pill').attr('class', 'nav-link');
    $('#title').html('Geriatrics | Search Results');
    $('#heading').html('Geriatrics | Search Results');
    $('#subheading').html('Search Results');
    return false;
  }
  
  $('#search-form').submit(searchSubmit);
 
  function pageClick(obj, url) {
    let pageLinks = document.querySelectorAll('.page-link');
    
    pageLinks.forEach(pageLink => {
    	let pageNum = `${pageLink.href}`.split('/').slice(-1);
    	
    	pageLink.addEventListener('click', e => {
    	  e.preventDefault();
    	  $(obj).load('admissions/' + pageNum, () => {
    	    pageClick(obj, url);
    	  });
    	});
    });	
  }
  
  $(insertHere).load(urlPath + 'page=1', () => {
    pageClick(insertHere, urlPath);
  });  
  
  const admissions = document.querySelector('#admissions_pill');
  const discharged = document.querySelector('#discharged_pill');
  
  admissions.addEventListener('click', () => {
    $(insertHere).load(urlPath + 'page=1', () => {
      pageClick(insertHere, urlPath);
      $('#admissions_pill').attr('class', 'nav-link active');
      $('#discharged_pill').attr('class', 'nav-link');
      $('#title').html('Geriatrics | Admissions');
      $('#heading').html('Geriatrics | Admissions');
      $('#subheading').html('All Admissions');      
    });
  });
  
  discharged.addEventListener('click', () => {
    $(insertHere).load(urlPathDC + 'page=1', () => {
      pageClick(insertHere, urlPathDC);
      $('#admissions_pill').attr('class', 'nav-link');
      $('#discharged_pill').attr('class', 'nav-link active');
      $('#title').html('Geriatrics | Discharged');
      $('#heading').html('Geriatrics | Discharged');
      $('#subheading').html('All Discharged');
    });
  });
});