document.addEventListener('DOMContentLoaded', () => {
    const id = document.querySelector('#admission').dataset.id;
    const monitoring = document.querySelector('#v-tabs-monitoring-tab'); 
    const management = document.querySelector('#v-tabs-management-tab');  
    
    const spinner1 = $('.spinner1');
    spinner1.hide();
    
    const spinner2 = $('.spinner2');
    spinner2.hide();
    
    function pageClick(obj, url) {    	   	
    	let page_links = document.querySelectorAll('.page-link');
    	     
      page_links.forEach(page_link => {
        let npage = `${page_link.href}`.split('/').slice(-1);
        
        page_link.addEventListener('click', e => {
    	    e.preventDefault();
    	    spinner2.show();
          $(obj).load(url + id + npage, () => {
            pageClick(obj, url);
            spinner2.hide();    	  
    	    });
    	  });
      });   
    };
    
    monitoring.addEventListener('click', () => {
    	// Injects the following tablists via AJAX    	
    	spinner1.show();
    	    	
      let vitals = '#vitalsigns';
    	let vitals_url = '/geriatrics/monitoring/vs/'; 
    	$(vitals).load(vitals_url + id, () => {
    		pageClick(vitals, vitals_url);    		
    	   spinner1.hide();   
    	   
    	   $('#create-vs').modalForm({
           formURL: "/geriatrics/monitoring/vs/create/" + id,
           modalID: "#monitoring-modal"
         }); 		
    	});  	
    	
    	let a = document.querySelector('#monitoring-tab-1');
    	a.addEventListener('click', () => {
    		spinner1.show();
    		let vitals = '#vitalsigns';
       	let vitals_url = '/geriatrics/monitoring/vs/'; 
       	$(vitals).load(vitals_url + id, () => {
    	   	pageClick(vitals, vitals_url);
    	   	spinner1.hide();
    	   });
    	});
    	
    	let b = document.querySelector('#monitoring-tab-2');
    	b.addEventListener('click', () => {
    		spinner1.show();
    		let fluids = '#fluids';
    	   let fluids_url = '/geriatrics/monitoring/fluids/';     	
    	   $(fluids).load(fluids_url + id, () => {
    	   	pageClick(fluids, fluids_url);
    	   	spinner1.hide();
      	});
    	});
    	
    	let c = document.querySelector('#monitoring-tab-3');
    	c.addEventListener('click', () => {
    		spinner1.show();
    		let turning = '#turning';
      	let turning_url = '/geriatrics/monitoring/turning/';
      	$(turning).load(turning_url + id, () => {
      		pageClick(turning, turning_url);
      		spinner1.hide();
      	});
    	});
    });
    
    management.addEventListener('click', () => {   
      spinner1.show(); 	
      
    	let meds = '#medications';
    	let meds_url = '/geriatrics/management/meds/';
    	
    	$(meds).load(meds_url + id, () => {
    	  pageClick(meds, meds_url);
    	  spinner1.hide();
    	  
    	  $('#create-medication').modalForm({
          formURL: "/geriatrics/management/meds/create/" + id,
          modalID: "#management-modal"
        });
        
        $('#create-planofcare').modalForm({
          formURL: "/geriatrics/management/plan_of_care/create/" + id,
          modalID: "#management-modal"
        });
        
        $('#create-problem').modalForm({
          formURL: "/geriatrics/management/probs/create/" + id,
          modalID: "#management-modal"
        });
        
    	});
    	
    	let medications = document.querySelector('#management-tab-1');
    	medications.addEventListener('click', () => {
    	  spinner1.show();
    	  let meds = '#medications';
    	  let meds_url = '/geriatrics/management/meds/';
    	  $(meds).load(meds_url + id, () => {
    	    pageClick(meds, meds_url);
    	    spinner1.hide();
    	  }); 
    	});
    	
    	let planofcare = document.querySelector('#management-tab-2');
    	planofcare.addEventListener('click', () => {
    	  spinner1.show();
    	  let plan = '#planofcare';
    	  let plan_url = '/geriatrics/management/plan_of_care/';
    	  $(plan).load(plan_url + id, () => {
    	    pageClick(plan, plan_url);
    	    spinner1.hide();
    	  });
      });
      
      let problems = document.querySelector('#management-tab-3');      
    	problems.addEventListener('click', () => {
    	  spinner1.show();
    	  let probs = '#problems';
    	  let probs_url = '/geriatrics/management/probs/';
    	  $(probs).load(probs_url + id, () => {
    	    pageClick(probs, probs_url);
    	    spinner1.hide();
    	  });
      });
    });
    
    const insertHere = document.querySelector('#insert-here');
    
    function searchSubmit() {
  	   console.log('running search');
      let query = $('#id_query').val();
      $(insertHere).load('admissions/?query=' + encodeURIComponent(query));
      $('#admissions_pill').attr('class', 'nav-link');
      $('#discharged_pill').attr('class', 'nav-link');
      $('#title').html('Geriatrics | Search Results');
      $('#heading').html('Geriatrics | Search Results');
      $('#subheading').html('Search Results');
      return false;
    }
  
    $('#search-form').submit(searchSubmit);
  
    const admissions = document.querySelector('#admissions_pill');
    const discharged = document.querySelector('#discharged_pill');
  
    admissions.addEventListener('click', () => {
      $(insertHere).load(urlPath + 'page=1', () => {
        pageClick(insertHere, urlPath);
        $('#admissions_pill').attr('class', 'nav-link active');
        $('#discharged_pill').attr('class', 'nav-link');
        $('#title').html('Geriatrics | Admissions');
        $('#heading').html('Geriatrics | Admissions');
        $('#subheading').html('All Admissions');      
      });
    });
  
    discharged.addEventListener('click', () => {
      $(insertHere).load(urlPathDC + 'page=1', () => {
        pageClick(insertHere, urlPathDC);
        $('#admissions_pill').attr('class', 'nav-link');
        $('#discharged_pill').attr('class', 'nav-link active');
        $('#title').html('Geriatrics | Discharged');
        $('#heading').html('Geriatrics | Discharged');
        $('#subheading').html('All Discharged');
      });
    });
});
      
    